
<!-- README.md is generated from README.Rmd. Please edit that file -->

# Machine learning task for SL36925 post

We provide you with a
[dataset](https://gitlab.com/evigorito/ml_task/-/blob/main/data.csv)
with 100 observations from the following variables:

1.  Outcome variable Y1
2.  Outcome variable Y2
3.  Predictor variables X1 to X100

Your task:

1.  Choose two ML algorithms to predict Y1 and Y2, using the predictors
    X1:X100.
2.  Briefly interpret your results.
3.  Did you find one outcome more difficult to predict than the other?
    why?

Use whichever ML algorithms you think might be appropriate, and
whichever language you prefer. Please **don’t spend more than 1 hour.**
Please include basic plots if relevant, but don’t do any fancy plots or
spend too much time writing, just a preliminary/exploratory analysis and
a basic conclusion. Please present your results in whichever way you
like. We like Rmarkdown or ipython notebooks, but if you aren’t familiar
with these, a word doc will suffice. If you are using a word doc, please
also send your code in a separate file, and if you are using
Rmarkdown/ipynb please don’t hide the code chunks.
